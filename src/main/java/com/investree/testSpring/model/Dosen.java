package com.investree.testSpring.model;

import jakarta.persistence.*;
import lombok.Data;

@Data
@Entity
@Table(name = "dosen")
public class Dosen {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    String name;
}
